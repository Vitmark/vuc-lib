/**
 * @file debounce.hpp
 *
 * @par
 * Copyright (c) 2024 Vít Vaněček
 * This code is licensed under MIT license (see LICENCE for details)
 */
#pragma once
#include <stddef.h>
#include <stdint.h>

#include "etl/iterator.h"

namespace vuc {

template <typename TCount = int32_t>
struct debounce {
  TCount valid_count{1};
  TCount count{0};
  bool state{false};

  bool add(bool sample) {
    if (sample != state && count < valid_count) {
      count += 1;
    } else if (sample != state) {
      count = 1;
    } else if (sample == state) {
      count = 0;
    }

    if (count_reached()) {
      state = sample;
      return true;
    }

    return false;
  }

  bool has_changed() const {
    return count_reached();
  }

  bool is_set() const {
    return state;
  }

  bool count_reached() const {
    return count >= valid_count;
  }
};

template <size_t ValidCount, typename TCount = int32_t>
struct cdebounce {
  static constexpr TCount valid_count{ValidCount};
  TCount count{0};
  bool state{false};

  bool add(bool sample) {
    if (sample != state && count < valid_count) {
      count += 1;
    } else if (sample != state) {
      count = 1;
    } else if (sample == state) {
      count = 0;
    }

    if (count_reached()) {
      state = sample;
      return true;
    }

    return false;
  }

  bool has_changed() const {
    return count_reached();
  }

  bool is_set() const {
    return state;
  }

  bool count_reached() const {
    return count >= valid_count;
  }
};

}  // namespace vuc
