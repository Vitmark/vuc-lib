/**
 * @file timing.hpp
 * @brief Class for timing periodic tasks without blocking delay
 *
 * @par
 * Copyright (c) 2024 Vít Vaněček
 * This code is licensed under MIT license (see LICENCE for details)
 */
#pragma once

namespace vuc {
inline namespace timing {

template <typename TTime, bool ONESHOT = false, bool OVERSHOOT_CORRECTION = false>
class time_keeper {
  static_assert(!(ONESHOT && OVERSHOOT_CORRECTION), "OVERSHOOT_CORRECTION cannot be enabled together with ONESHOT");

 public:
  explicit constexpr time_keeper(const TTime update_period, const TTime current_time = 0) : update_period{update_period}, timestamp{current_time} {
  }

  TTime get_time_passed(const TTime current_time) const {
    return current_time - timestamp;
  }

  bool needs_update(const TTime current_time) const {
    return (update_period == 0) || (get_time_passed(current_time) >= update_period);
  }

  bool update(const TTime current_time) {
    if (ONESHOT && finished) {
      return true;
    }
    if (needs_update(current_time)) {
      if (ONESHOT) {
        finished = true;
      } else if (OVERSHOOT_CORRECTION) {
        timestamp += update_period;
      } else {
        timestamp = current_time;
      }
      return true;
    }
    return false;
  }

  void set_update_period(const TTime new_update_period) {
    update_period = new_update_period;
  }

  TTime get_update_period() const {
    return update_period;
  }

  void reset(const TTime current_time = 0) {
    timestamp = current_time;
    if (ONESHOT) {
      finished = false;
    }
  }

  void reset_new_period(const TTime new_update_period, const TTime current_time = 0) {
    reset(current_time);
    set_update_period(new_update_period);
  }

 private:
  TTime update_period{0};
  TTime timestamp{0};
  bool finished{false};
};

template <typename TTime>
struct periodic_timer {
  TTime get_time_passed(const TTime current_time) const {
    return current_time - timestamp;
  }

  bool needs_update(const TTime current_time) const {
    return (period == 0) || (get_time_passed(current_time) >= period);
  }

  bool update(const TTime current_time) {
    if (needs_update(current_time)) {
      timestamp = current_time;
      return true;
    }
    return false;
  }

  void reset(const TTime current_time = 0) {
    timestamp = current_time;
  }

  void reset_new_period(const TTime new_update_period, const TTime current_time = 0) {
    reset(current_time);
    period = new_update_period;
  }

  TTime period{0};
  TTime timestamp{0};
};

template <typename TTime>
struct oneshot_timer {
  TTime get_time_passed(const TTime current_time) const {
    return current_time - timestamp;
  }

  bool needs_update(const TTime current_time) const {
    return (period == 0) || (get_time_passed(current_time) >= period);
  }

  bool update(const TTime current_time) {
    if (!finished && needs_update(current_time)) {
      finished = true;
    }
    return finished;
  }

  void reset(const TTime current_time = 0) {
    timestamp = current_time;
    finished  = false;
  }

  void reset_new_period(const TTime new_update_period, const TTime current_time = 0) {
    reset(current_time);
    period = new_update_period;
  }

  TTime period{0};
  TTime timestamp{0};
  bool finished{false};
};

}  // namespace timing
}  // namespace vuc
