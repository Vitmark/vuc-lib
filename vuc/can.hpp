/**
 * @file can.hpp
 * @brief Basic definitions for can
 *
 * @par
 * Copyright (c) 2024 Vít Vaněček
 * This code is licensed under MIT license (see LICENCE for details)
 */
#pragma once
#include <etl/algorithm.h>
#include <etl/array.h>
#include <stddef.h>
#include <stdint.h>

namespace vuc {
inline namespace can {

struct can_id {
  inline static constexpr uint32_t ide_bit_num = 31;
  inline static constexpr uint32_t rtr_bit_num = 30;
  inline static constexpr uint32_t ide_flag    = 1 << ide_bit_num;
  inline static constexpr uint32_t rtr_flag    = 1 << rtr_bit_num;
  inline static constexpr uint32_t sf_mask     = (1 << 11) - 1;
  inline static constexpr uint32_t ef_mask     = (1 << 29) - 1;

  static constexpr bool must_be_extended(const uint32_t id) {
    return (id & ef_mask) > sf_mask;
  }

  static constexpr uint32_t construct_ident(const uint32_t id, const bool force_extended, const bool remote_frame) {
    const uint32_t ide = static_cast<uint32_t>(force_extended || must_be_extended(id)) << ide_bit_num;
    const uint32_t rtr = static_cast<uint32_t>(remote_frame) << rtr_bit_num;
    return (id & ef_mask) | ide | rtr;
  }

  can_id() = default;

  constexpr can_id(uint32_t id, bool force_extended = false, bool remote_frame = false) : ident{construct_ident(id, force_extended, remote_frame)} {
  }

  constexpr bool operator==(const can_id& other) const {
    return ident == other.ident;
  };

  constexpr can_id& set_id(uint32_t id, bool force_extended, bool remote_frame) {
    ident = construct_ident(id, force_extended, remote_frame);
    return *this;
  }
  constexpr can_id& set_id(uint32_t id, bool force_extended) {
    return set_id(id, force_extended, is_remote());
  }
  constexpr can_id& set_id(uint32_t id) {
    return set_id(id, is_extended(), is_remote());
    return *this;
  }

  constexpr can_id& set_rtr(bool rtr) {
    if (rtr) {
      ident |= rtr_flag;
    } else {
      ident &= ~rtr_flag;
    }
    return *this;
  }

  constexpr can_id& increment_id(uint32_t increment) {
    return set_id(get_id() + increment);
  }
  constexpr can_id new_with_increment(uint32_t increment) const {
    return can_id(get_id() + increment, is_extended(), is_remote());
  }

  constexpr uint32_t get_id() const {
    return ident & ef_mask;
  }
  constexpr bool is_standard() const {
    return !is_extended();
  }
  constexpr bool is_extended() const {
    return (ident & ide_flag) == ide_flag;
  }
  /** @brief Is data frame. */
  constexpr bool is_data() const {
    return !is_remote();
  }
  /** @brief Is remote request frame. */
  constexpr bool is_remote() const {
    return (ident & rtr_flag) == rtr_flag;
  }

  uint32_t ident;
};

using can_data = etl::array<uint8_t, 8>;
struct can_frame {
  inline static constexpr uint8_t dlc_max = 8;

  constexpr bool operator==(const can_frame& other) const {
    return id == other.id && len == other.len && data == other.data;
  }
  constexpr bool operator!=(const can_frame& other) const {
    return !(*this == other);
  }

  // Return CAN DLC limited to 8
  constexpr uint8_t get_dlc() const {
    return (etl::min)(len, dlc_max);
  }

  can_id id;
  uint8_t len;
  // 3B padding
  [[gnu::aligned(8)]] can_data data;
};

using canfd_data = etl::array<uint8_t, 64>;
struct canfd_frame {
  // Mode flags
  inline static constexpr size_t fd_bit_num  = 0;
  inline static constexpr size_t fd_flag     = 1 << fd_bit_num;
  inline static constexpr size_t brs_bit_num = 1;
  inline static constexpr size_t brs_flag    = 1 << brs_bit_num;
  inline static constexpr uint8_t dlc_mask   = 0x0FU;
  inline static constexpr uint8_t dlc_max    = 15;
  inline static constexpr etl::array<uint8_t, 16> dlc_lookup{0, 1, 2, 3, 4, 5, 6, 7, 8, 12, 16, 20, 24, 32, 48, 64};

  constexpr bool operator==(const canfd_frame& other) const {
    return id == other.id && len == other.len && mode == other.mode && data == other.data;
  }

  constexpr bool operator!=(const canfd_frame& other) const {
    return !(*this == other);
  }

  static constexpr uint8_t dlc_to_len(uint8_t dlc) {
    return dlc_lookup[dlc & dlc_mask];
  }

  static constexpr uint8_t len_to_dlc(const uint8_t len) {
    const uint8_t* const lookup_ind = etl::lower_bound(dlc_lookup._buffer, dlc_lookup.end(), len);
    const uint8_t ind               = etl::distance(dlc_lookup._buffer, lookup_ind);
    return (etl::min)(ind, dlc_max);
  }

  constexpr uint8_t get_dlc() const {
    return len_to_dlc(len);
  }

  can_id id;
  uint8_t len;
  uint8_t mode;
  // 2B padding
  [[gnu::aligned(8)]] canfd_data data;
};

}  // namespace can
}  // namespace vuc
