/**
 * @file i2c_eeprom.hpp
 * @brief Class for communication with I2C EEPROM
 *
 * @par
 * Copyright (c) 2024 Vít Vaněček
 * This code is licensed under MIT license (see LICENCE for details)
 */
#pragma once

#include <stddef.h>
#include <stdint.h>

#include "etl/expected.h"
#include "etl/span.h"

namespace vuc {

enum class i2c_eeprom_size : size_t {
  E24LC01  = 1 << 7,
  E24LC02  = 1 << 8,
  E24LC04  = 1 << 9,
  E24LC08  = 1 << 10,
  E24LC16  = 1 << 11,
  E24LC32  = 1 << 12,
  E24LC64  = 1 << 13,
  E24LC128 = 1 << 14,
  E24LC256 = 1 << 15,
  E24LC512 = 1 << 16,
};

template <typename TI2C, i2c_eeprom_size DeviceSize>
class i2c_eeprom {
  static constexpr size_t determine_page_size(i2c_eeprom_size eeprom_size) {
    switch (eeprom_size) {
      case i2c_eeprom_size::E24LC01:
      case i2c_eeprom_size::E24LC02:
        return 8;

      case i2c_eeprom_size::E24LC04:
      case i2c_eeprom_size::E24LC08:
      case i2c_eeprom_size::E24LC16:
        return 16;

      case i2c_eeprom_size::E24LC32:
      case i2c_eeprom_size::E24LC64:
        return 32;

      case i2c_eeprom_size::E24LC128:
      case i2c_eeprom_size::E24LC256:
        return 64;

      case i2c_eeprom_size::E24LC512:
        return 128;
    }
  }

  static constexpr size_t determine_address_width(i2c_eeprom_size eeprom_size) {
    switch (eeprom_size) {
      case i2c_eeprom_size::E24LC01:
      case i2c_eeprom_size::E24LC02:
      case i2c_eeprom_size::E24LC04:
      case i2c_eeprom_size::E24LC08:
      case i2c_eeprom_size::E24LC16:
        return 1;

      case i2c_eeprom_size::E24LC32:
      case i2c_eeprom_size::E24LC64:
      case i2c_eeprom_size::E24LC128:
      case i2c_eeprom_size::E24LC256:
      case i2c_eeprom_size::E24LC512:
        return 2;
    }
  }

 public:
  enum class error {
    arguments = 1,
    i2c_read,
    i2c_write,
    not_enought_space,
  };

  static constexpr size_t device_size   = static_cast<size_t>(DeviceSize);
  static constexpr size_t page_size     = determine_page_size(DeviceSize);
  static constexpr size_t address_width = determine_address_width(DeviceSize);

  constexpr i2c_eeprom(TI2C& writer_, uint8_t i2c_address_) : writer(writer_), i2c_address(i2c_address_) {
  }

  etl::expected<void, error> write(const uint32_t address, etl::span<const uint8_t> data) {
    if (address + data.size() > device_size) {
      return etl::unexpected(error::not_enought_space);
    }

    size_t data_written{0};
    while (data_written < data.size()) {
      auto result = write_till_page_boundary(address + data_written, data.subspan(data_written));
      if (!result) return etl::unexpected(result.error());
      data_written += result.value();
    }
    return {};
  }

  etl::expected<size_t, error> write_till_page_boundary(const uint32_t address, etl::span<const uint8_t> data) {
    const size_t next_page_boundary = get_bytes_till_page_boundary(address);
    const size_t bytes_to_write     = (etl::min)(next_page_boundary, data.size());

    auto result = unchecked_write(address, data.subspan(0, bytes_to_write));
    if (!result) return etl::unexpected(result.error());
    return bytes_to_write;
  }

  etl::expected<void, error> read(const uint32_t address, etl::span<uint8_t> data) {
    if (address + data.size() > device_size) {
      return etl::unexpected(error::not_enought_space);
    }
    {
      const auto result = write_address(address);
      if (!result) return etl::unexpected(result.error());
    }
    if (writer.read(i2c_address, data) != data.size()) {
      return etl::unexpected(error::i2c_read);
    }
    return {};
  }

  static constexpr size_t get_bytes_till_page_boundary(uint16_t address) {
    return page_size - (address % page_size);
  }

 private:
  etl::expected<void, error> write_address(const uint32_t address) {
    if constexpr (address_width == 1) {
      if (writer.write(i2c_address, static_cast<uint8_t>(address & 0xFF)) != 1) {
        return unexpected(error::i2c_write);
      }
    } else if constexpr (address_width == 2) {
      const etl::array<uint8_t, 2> address_raw{(uint8_t)((address >> 8) & 0xFF), (uint8_t)(address & 0xFF)};
      if (writer.write(i2c_address, address_raw) != address_raw.size()) {
        return etl::unexpected(error::i2c_write);
      }
    }
    return {};
  }

  etl::expected<void, error> unchecked_write(const uint32_t address, etl::span<const uint8_t> data) {
    if (writer.write_mem(i2c_address, data, address, address_width) != data.size()) {
      return etl::unexpected(error::i2c_write);
    }
    return {};
  }

 private:
  TI2C& writer;
  uint8_t i2c_address;
};

}  // namespace vuc
