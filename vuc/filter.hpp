/**
 * @file filter.hpp
 * @brief Classes for filtering values
 *
 * @par
 * Copyright (c) 2024 Vít Vaněček
 * This code is licensed under MIT license (see LICENSE for details)
 */
#pragma once

#include <stddef.h>

#include "etl/algorithm.h"
#include "etl/circular_buffer.h"

namespace vuc {
inline namespace filter {

template <typename TVal, typename TSum, size_t WINDOW>
class moving_average {
 public:
  void add_value(const TVal& new_value) {
    if (avg_buffer.full()) {
      const auto& oldest_value = avg_buffer.front();
      sum -= oldest_value;
    }

    avg_buffer.push(new_value);
    sum += new_value;
  }

  TVal calculate() const {
    return sum / static_cast<TSum>((etl::max)(size_t(1), avg_buffer.size()));
  }

  const TVal& get_newest_values() const {
    return avg_buffer.back();
  }

  size_t avg_buffer_size() const {
    return avg_buffer.size();
  }

 private:
  etl::circular_buffer<TVal, WINDOW> avg_buffer{};
  TSum sum{};
};

}  // namespace filter
}  // namespace vuc
