/**
 * @file encode_decode.hpp
 * @brief Functions to encode and decode basic types
 *
 * @par
 * Copyright (c) 2024 Vít Vaněček
 * This code is licensed under MIT license (see LICENCE for details)
 */
#pragma once
#include <etl/array.h>
#include <etl/byte_stream.h>
#include <etl/endianness.h>
#include <etl/private/dynamic_extent.h>
#include <etl/span.h>
#include <stddef.h>

namespace vuc {
template <typename... Ts>
struct count_bytes;

template <>
struct count_bytes<> {
  inline static constexpr size_t value{0};
};

template <typename T, size_t Extent>
struct count_bytes<etl::span<T, Extent>> {
  static_assert(Extent != etl::dynamic_extent, "Variable size span not supported");
  inline static constexpr size_t value{sizeof(T) * Extent};
};

template <typename T, size_t Size>
struct count_bytes<etl::array<T, Size>> {
  inline static constexpr size_t value{sizeof(T) * Size};
};

template <typename T>
struct count_bytes<T> {
  inline static constexpr size_t value{sizeof(T)};
};

template <typename T, typename... Ts>
struct count_bytes<T, Ts...> {
  inline static constexpr size_t value = count_bytes<T>::value + count_bytes<Ts...>::value;
};

template <typename... Ts>
inline constexpr size_t count_bytes_v = count_bytes<Ts...>::value;

template <typename T, size_t Extent>
inline size_t encode_stream(etl::byte_stream_writer &stream, const etl::span<T, Extent> &value) {
  return stream.write(value.subspan(0)) ? value.size() * sizeof(T) : 0;
}

template <typename T, size_t SIZE>
inline size_t encode_stream(etl::byte_stream_writer &stream, const etl::array<T, SIZE> &value) {
  return stream.write(etl::span<const T>{value}) ? SIZE * sizeof(T) : 0;
}

template <typename T>
inline size_t encode_stream(etl::byte_stream_writer &stream, const T &value) {
  return stream.write(value) ? sizeof(T) : 0;
}

template <typename T, typename... Ts>
inline size_t encode_stream(etl::byte_stream_writer &stream, const T &value, const Ts &...values) {
  return encode_stream(stream, value) + encode_stream(stream, values...);
}

template <typename T>
inline bool decode_stream(etl::byte_stream_reader &stream, T &value) {
  const auto temp = stream.read<T>();
  if (!temp.has_value()) return false;
  value = temp.value();
  return true;
}

template <typename T, size_t Extent>
inline bool decode_stream(etl::byte_stream_reader &stream, etl::span<T, Extent> &value) {
  const auto temp = stream.read<T>(value);
  if (!temp.has_value()) return false;
  return true;
}

template <typename T, size_t SIZE>
inline bool decode_stream(etl::byte_stream_reader &stream, etl::array<T, SIZE> &value) {
  const auto temp = stream.read<T>(value);
  if (!temp.has_value()) return false;
  return true;
}

template <typename T, typename... Ts>
inline bool decode_stream(etl::byte_stream_reader &stream, T &value, Ts &...values) {
  return decode_stream(stream, value) && decode_stream(stream, values...);
}

template <typename... Ts>
inline size_t encode_byte_array(const etl::span<uint8_t> data, const etl::endian endianness, const size_t byte_offset, const Ts &...values) {
  etl::byte_stream_writer stream(data.begin() + byte_offset, data.end(), endianness);
  return encode_stream(stream, values...);
}

template <typename... Ts>
inline size_t decode_byte_array(const etl::span<const uint8_t> data, const etl::endian endianness, const size_t byte_offset, Ts &...values) {
  etl::byte_stream_reader stream(data.begin() + byte_offset, data.end(), endianness);
  return decode_stream(stream, values...);
}

}  // namespace vuc
