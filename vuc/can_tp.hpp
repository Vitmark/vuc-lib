/**
 * @file can_tp.hpp
 * @brief CAN TP
 *
 * @par
 * Copyright (c) 2024 Vít Vaněček
 * This code is licensed under MIT license (see LICENCE for details)
 */
#pragma once
#include <etl/algorithm.h>
#include <etl/array.h>
#include <etl/optional.h>
#include <etl/span.h>
#include <stddef.h>
#include <stdint.h>

#include <cstdint>

#include "can.hpp"
#include "process_can.hpp"

namespace vuc {
inline namespace can {
namespace tp {

inline constexpr uint8_t padding_byte{0xAA};

enum class frame_type : uint8_t {
  Single      = 0,
  First       = 1,
  Consecutive = 2,
  Flow        = 3,
};

enum class flow_flag : uint8_t {
  ContinueToSend = 0,
  Wait           = 1,
  Abort          = 2,
};

using separation_time_t = uint8_t;

inline constexpr uint32_t separation_time_to_microseconds(separation_time_t separation_time) {
  if (separation_time <= 0x7F) {
    return separation_time * 1000;
  } else if (separation_time >= 0xF1 && separation_time <= 0xF9) {
    return (separation_time & 0x0F) * 100;
  } else {
    return 0;
  }
}

inline constexpr uint8_t get_sf_control_byte(uint8_t size) {
  return static_cast<uint8_t>(frame_type::Single) << 4 | (size & 0x07);
}

inline constexpr etl::array<uint8_t, 2> get_ff_control_bytes(uint16_t size) {
  const uint8_t first  = static_cast<uint8_t>(frame_type::First) << 4 | ((size >> 8) & 0x0F);
  const uint8_t second = size & 0xFF;
  return {first, second};
}

inline constexpr uint8_t get_cf_control_byte(uint8_t index) {
  return static_cast<uint8_t>(frame_type::Consecutive) << 4 | (index & 0x0F);
}

inline constexpr uint8_t get_fc_control_byte(flow_flag flag) {
  return static_cast<uint8_t>(frame_type::Flow) << 4 | static_cast<uint8_t>(flag);
}

inline etl::optional<frame_type> decode_frame_type(const can_frame& frame) {
  if (frame.id.is_remote() || frame.get_dlc() != 8) {
    return etl::nullopt;
  }
  uint8_t control_byte = frame.data[0] >> 4;
  if (control_byte <= static_cast<uint8_t>(frame_type::Flow)) {
    return static_cast<frame_type>(control_byte);
  }
  return etl::nullopt;
}

inline bool decode_single_frame(const can_frame& frame, etl::span<uint8_t> out_data, uint32_t& data_size) {
  const uint8_t dsize = frame.data[0] & 0x0F;
  if (dsize > 7 || dsize > out_data.size()) {
    return false;
  }
  data_size = dsize;
  etl::copy_n(frame.data.begin() + 1, dsize, out_data.begin());
  return true;
}

inline int decode_first_frame_size(const can_frame& frame, uint32_t& data_size) {
  uint16_t dsize = frame.data[1];
  dsize |= (frame.data[0] & 0x0F) << 8;

  uint32_t extended_size;

  if (dsize == 0) {
    vuc::can::decode_can_data_offset(frame.data, etl::endian::little, 2, extended_size);
    if (extended_size <= 4095) {
      return -1;
    }
    data_size = extended_size;
    return 2;
  } else {
    if (dsize < 8) {
      return -1;
    }
    data_size = dsize;
    return 6;
  }
}

inline int decode_first_frame(const can_frame& frame, etl::span<uint8_t> out_data, uint32_t& data_size) {
  uint32_t dsize;

  const int initial_data_size = decode_first_frame_size(frame, dsize);
  if (initial_data_size < 0) {
    return initial_data_size;
  }

  data_size              = dsize;
  const size_t copy_size = (etl::min<size_t>)(initial_data_size, out_data.size());
  etl::copy_n(frame.data.end() - initial_data_size, copy_size, out_data.begin());
  return copy_size;
}

inline bool decode_consecutive_frame(const can_frame& frame, etl::span<uint8_t> out_data, uint8_t& index) {
  const uint8_t temp_index = frame.data[0] & 0x0F;
  index                    = temp_index;
  etl::copy_n(frame.data.begin() + 1, (etl::min<size_t>)(7, out_data.size()), out_data.begin());
  return true;
}

inline bool decode_consecutive_frame_index(const can_frame& frame, uint8_t& index) {
  const uint8_t temp_index = frame.data[0] & 0x0F;
  index                    = temp_index;
  return true;
}

inline bool decode_flow_frame(const can_frame& frame, flow_flag& flag, uint8_t& block_size, separation_time_t& separation_time) {
  const uint8_t raw_flag = frame.data[0] & 0x0F;

  if (raw_flag > static_cast<uint8_t>(flow_flag::Abort)) {
    return false;
  }

  flag            = static_cast<flow_flag>(raw_flag);
  block_size      = frame.data[1];
  separation_time = frame.data[2];
  return true;
}

inline void encode_single_frame(can_frame& out_frame, const etl::span<const uint8_t> data) {
  out_frame.len = 8;
  out_frame.data.fill(padding_byte);
  const uint8_t data_size = (etl::min<size_t>)(data.size(), 7);
  out_frame.data[0]       = get_sf_control_byte(data_size);
  etl::copy_n(data.begin(), data_size, out_frame.data.begin() + 1);
}

inline size_t encode_first_frame(can_frame& out_frame, const etl::span<const uint8_t> data, uint32_t overall_data_size) {
  out_frame.len = 8;
  out_frame.data.fill(padding_byte);

  uint32_t dsize;
  uint32_t extended_size;
  if (overall_data_size <= 4095) {
    dsize         = overall_data_size;
    extended_size = 0;
  } else {
    dsize         = 0;
    extended_size = overall_data_size;
  }

  const auto control = get_ff_control_bytes(dsize);
  size_t copy_count;
  if (extended_size != 0) {
    vuc::can::encode_to_can_data(out_frame.data, control[0], control[1], extended_size);
    copy_count = (etl::min<size_t>)(2, data.size());
    etl::copy_n(data.begin(), copy_count, out_frame.data.begin() + 6);
  } else {
    vuc::can::encode_to_can_data(out_frame.data, control[0], control[1]);
    copy_count = (etl::min<size_t>)(6, data.size());
    etl::copy_n(data.begin(), copy_count, out_frame.data.begin() + 2);
  }
  return copy_count;
}

inline size_t encode_first_frame(can_frame& out_frame, const etl::span<const uint8_t> data) {
  return encode_first_frame(out_frame, data, data.size());
}

inline size_t encode_consecutive_frame(can_frame& out_frame, const etl::span<const uint8_t> data, uint8_t index) {
  out_frame.len = 8;
  out_frame.data.fill(padding_byte);
  out_frame.data[0]  = get_cf_control_byte(index);
  const size_t count = (etl::min<size_t>)(7, data.size());
  etl::copy_n(data.begin(), count, out_frame.data.begin() + 1);
  return count;
}

inline void encode_flow_frame(can_frame& out_frame, flow_flag flag, uint8_t block_size, separation_time_t separation_time) {
  out_frame.len = 8;
  out_frame.data.fill(padding_byte);
  out_frame.data[0] = get_fc_control_byte(flag);
  out_frame.data[1] = block_size;
  out_frame.data[2] = separation_time;
}

struct tx_channel {
  etl::span<const uint8_t> data_to_send{};
  can_id tx_can_id;
  uint16_t sent_size{0};
  uint8_t next_frame_index{0};
  uint8_t remaining_block{0};
  separation_time_t separation_time{0};
  bool no_flow{false};

  void reset() {
    sent_size        = 0;
    next_frame_index = 0;
    remaining_block  = 0;
    no_flow          = false;
    separation_time  = 0;
  }

  void reset_new_data(etl::span<uint8_t> new_data) {
    reset();
    data_to_send = new_data;
  }

  size_t get_remaining_size() const {
    return data_to_send.size() - sent_size;
  }

  bool finished() const {
    return get_remaining_size() == 0;
  }

  bool started() const {
    return sent_size > 0;
  }

  void wait() {
    remaining_block = 0;
    no_flow         = false;
  }

  void abort() {
    sent_size = data_to_send.size();
  }

  uint32_t get_separation_time_uc() const {
    return separation_time_to_microseconds(separation_time);
  }

  void set_block_size(uint8_t block_size) {
    if (block_size == 0) {
      remaining_block = 0;
      no_flow         = true;
    } else {
      remaining_block = block_size;
      no_flow         = false;
    }
  }

  bool should_send_next() const {
    if (data_to_send.size() == 0 || finished()) {
      return false;
    } else if (no_flow || sent_size == 0) {
      return true;
    } else {
      return remaining_block > 0;
    }
  }

  bool encode_next_frame(can_frame& out_frame) {
    if (!started() && data_to_send.size() <= 7) {
      encode_single_frame(out_frame, data_to_send);
      abort();
    } else if (!started()) {
      sent_size        = encode_first_frame(out_frame, data_to_send);
      next_frame_index = 1;
      remaining_block  = 0;
    } else if (!finished()) {
      sent_size += encode_consecutive_frame(out_frame, data_to_send.subspan(sent_size), next_frame_index);
      next_frame_index = (next_frame_index + 1) & 0x0F;
      if (remaining_block > 0) {
        remaining_block -= 1;
      }
    } else {
      return false;
    }

    out_frame.id = tx_can_id;
    return true;
  }

  void handle_flow_frame(const vuc::can_frame& rx_frame) {
    flow_flag new_flag{};
    uint8_t block_size{};
    separation_time_t new_separation_time{};
    decode_flow_frame(rx_frame, new_flag, block_size, new_separation_time);
    if (new_flag == flow_flag::ContinueToSend) {
      set_block_size(block_size);
      separation_time = new_separation_time;
    } else if (new_flag == flow_flag::Wait) {
      wait();
    } else if (new_flag == flow_flag::Abort) {
      abort();
    }
  }
};

template <void (*send_frame_func)(const can_frame& tx_frame) = nullptr>
struct rx_channel {
  etl::span<uint8_t> receive_buffer;
  vuc::can_id tx_can_id{};

  uint32_t received_size{0};
  uint32_t expected_size{0};
  uint8_t previous_index{0};
  uint8_t remaining_block{0};

  separation_time_t rx_separation_time{0};
  uint8_t rx_block_size{0};

  void reset() {
    received_size   = 0;
    expected_size   = 0;
    previous_index  = 0;
    remaining_block = 0;
  }

  void reset(etl::span<uint8_t> new_buffer) {
    reset();
    receive_buffer = new_buffer;
  }

  uint32_t get_remaining_size() const {
    return expected_size - received_size;
  }

  bool finished() const {
    return started() && get_remaining_size() == 0;
  }

  bool started() const {
    return expected_size > 0;
  }

  etl::span<uint8_t> get_received_data() {
    return receive_buffer.subspan(0, received_size);
  }

  bool start_rx(can::can_data ff_data, uint32_t ff_size) {
    if (ff_size > receive_buffer.size() || ff_size < 8) {
      return false;
    }
    reset();
    expected_size = ff_size;
    uint32_t copy_size;
    if (ff_size <= 4095) {
      copy_size = 6;
      etl::copy_n(ff_data.begin() + 2, copy_size, receive_buffer.begin());
    } else {
      copy_size = 2;
      etl::copy_n(ff_data.begin() + 6, copy_size, receive_buffer.begin());
    }
    received_size = copy_size;
    return true;
  }

  bool continue_rx(can::can_data cf_data, uint8_t cf_index) {
    const uint8_t expected_index = (previous_index + 1) & 0x0F;
    if (finished()) {
      return true;
    } else if (cf_index != expected_index) {
      return false;
    }
    size_t copy_size = (etl::min<uint32_t>)(7, get_remaining_size());
    etl::copy_n(cf_data.begin() + 1, copy_size, receive_buffer.begin() + received_size);
    received_size += copy_size;
    previous_index = cf_index;
    return true;
  }

  void send_frame(const can_frame& tx_frame) {
    if constexpr (send_frame_func != nullptr) {
      send_frame_func(tx_frame);
    }
  }

  void send_abort() {
    can_frame tx_frame{
        .id  = tx_can_id,
        .len = 8,
        .data{},
    };
    encode_flow_frame(tx_frame, flow_flag::Abort, 0, 0);
    send_frame(tx_frame);
  }

  void send_continue() {
    can_frame tx_frame{
        .id  = tx_can_id,
        .len = 8,
        .data{},
    };
    encode_flow_frame(tx_frame, flow_flag::ContinueToSend, rx_block_size, rx_separation_time);
    send_frame(tx_frame);
  }

  bool handle_first_frame(const can_frame& rx_frame, const can_id id_for_tx) {
    if (finished()) {
      send_abort();
      return true;
    } else if (started()) {
      return true;
    } else {
      uint32_t ff_size{0};
      decode_first_frame_size(rx_frame, ff_size);
      bool sucess = start_rx(rx_frame.data, ff_size);
      if (sucess) {
        tx_can_id = id_for_tx;
        send_continue();
        remaining_block = rx_block_size;
      }
      return sucess;
    }
  }

  bool handle_consecutive_frame(const can_frame& rx_frame) {
    if (finished()) {
      return true;
    } else if (started()) {
      uint8_t cf_index{0xFF};
      decode_consecutive_frame_index(rx_frame, cf_index);
      bool sucess = continue_rx(rx_frame.data, cf_index);
      if (!sucess) {
        send_abort();
      } else if (remaining_block > 0) {
        if (remaining_block == 1) {
          send_continue();
          remaining_block = rx_block_size;
        } else {
          remaining_block -= 1;
        }
      }
      return sucess;
    } else {
      return true;
    }
  }
};

}  // namespace tp
}  // namespace can
}  // namespace vuc
