#pragma once
/**
 * @file limit.hpp
 *
 * @par
 * Copyright (c) 2024 Vít Vaněček
 * This code is licensed under MIT license (see LICENCE for details)
 */
#include "etl/algorithm.h"

namespace vuc {

template <typename T>
constexpr inline T value_in_limit(const T vmin, const T vmax, const T value) {
  return etl::min<T>(etl::max<T>(value, vmin), vmax);
}

}  // namespace vuc
