/**
 * @file hysteresis.hpp
 * @brief Classes for using hysteresis
 *
 * @par
 * Copyright (c) 2024 Vít Vaněček
 * This code is licensed under MIT license (see LICENCE for details)
 */
#pragma once

namespace vuc {

template <typename T>
class square_hysteresis {
 public:
  constexpr square_hysteresis(T set_point, T reset_point, bool initial_state = false) : set_point(set_point), reset_point(reset_point), state(initial_state) {
  }

  bool get_state() const {
    return state;
  }

  bool update(T value) {
    if (set_point >= reset_point) {
      if (state && value <= reset_point)
        state = false;
      else if (!state && value >= set_point)
        state = true;
    } else {
      if (state && value >= reset_point)
        state = false;
      else if (!state && value <= set_point)
        state = true;
    }
    return state;
  }

  void set_hysteresis_points(T set_point, T reset_point) {
    this->set_point   = set_point;
    this->reset_point = reset_point;
  }

 private:
  T set_point;
  T reset_point;
  bool state;
};

}  // namespace vuc
