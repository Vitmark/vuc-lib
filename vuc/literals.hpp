/**
 * @file literals.hpp
 * @brief Usefull integer literals
 *
 * @par
 * Copyright (c) 2024 Vít Vaněček
 * This code is licensed under MIT license (see LICENCE for details)
 */
#pragma once
#include <stddef.h>
#include <stdint.h>

namespace vuc {

inline namespace literals {
inline namespace integer_literals {

constexpr uint8_t operator""_u8(unsigned long long num) {
  return static_cast<uint8_t>(num);
}

constexpr int8_t operator""_i8(unsigned long long num) {
  return static_cast<int8_t>(num);
}

constexpr uint16_t operator""_u16(unsigned long long num) {
  return static_cast<uint16_t>(num);
}

constexpr int16_t operator""_i16(unsigned long long num) {
  return static_cast<int16_t>(num);
}

constexpr uint32_t operator""_u32(unsigned long long num) {
  return static_cast<uint32_t>(num);
}

constexpr int32_t operator""_i32(unsigned long long num) {
  return static_cast<int32_t>(num);
}

constexpr uint64_t operator""_u64(unsigned long long num) {
  return static_cast<uint64_t>(num);
}

constexpr int64_t operator""_i64(unsigned long long num) {
  return static_cast<int64_t>(num);
}

constexpr size_t operator""_uz(unsigned long long num) {
  return static_cast<size_t>(num);
}

}  // namespace integer_literals
}  // namespace literals
}  // namespace vuc
