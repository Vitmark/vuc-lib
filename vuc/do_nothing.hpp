/**
 * @file do_nothing.hpp
 *
 * @par
 * Copyright (c) 2024 Vít Vaněček
 * This code is licensed under MIT license (see LICENCE for details)
 */
#pragma once

#include <stdint.h>

namespace vuc {
/**
 * @brief Do nothing for 2 cycles * start_value
 */
[[gnu::always_inline]] inline void do_2nothing(uint32_t start_value) {
  if (start_value == 0) return;
  for (; start_value > 0; --start_value) {
    // Needed so that compiler doesn't optimize the empty loop away
    asm("");
  }
}

/**
 * @param[in] clock_mhz Processor clock in MHz
 * @param[in] nanos Time in nanosecond
 */
[[gnu::always_inline]] inline void do_nothing_ns(uint32_t clock_mhz, uint32_t nanos) {
  do_2nothing(clock_mhz * nanos / 1000 / 2);
}

/**
 * @param[in] clock_mhz Processor clock in MHz
 * @param[in] micros Time in microsecond
 */
[[gnu::always_inline]] inline void do_nothing_us(uint32_t clock_mhz, uint32_t micros) {
  do_2nothing(clock_mhz * micros / 2);
}
}  // namespace vuc
