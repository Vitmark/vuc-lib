/**
 * @file bytes.hpp
 *
 * @par
 * Copyright (c) 2024 Vít Vaněček
 * This code is licensed under MIT license (see LICENCE for details)
 */
#pragma once

#include <cstdint>

#include "etl/private/dynamic_extent.h"
#include "etl/span.h"
namespace vuc {

inline constexpr size_t dynamic_or_size(size_t n, size_t size) {
  if (n == etl::dynamic_extent) {
    return etl::dynamic_extent;
  } else {
    return size;
  }
}

template <typename T, size_t N>
inline constexpr etl::span<const uint8_t, dynamic_or_size(N, N * sizeof(T))> span_as_u8(etl::span<const T, N> s) {
  return etl::span<const uint8_t, dynamic_or_size(N, N * sizeof(T))>(reinterpret_cast<const uint8_t*>(s.data()), s.size_bytes());
}

template <typename T>
inline constexpr etl::span<const uint8_t, 1 * sizeof(T)> as_u8(const T& s) {
  return etl::span<const uint8_t, 1 * sizeof(T)>(reinterpret_cast<const uint8_t*>(&s), 1 * sizeof(T));
}

template <typename T>
inline constexpr etl::span<const uint32_t, sizeof(T) / sizeof(uint32_t)> as_u32(const T& s) {
  static_assert(alignof(T) >= alignof(uint32_t), "Alignment of type must be larger or equal to u32.");
  return etl::span<const uint32_t, sizeof(T) / sizeof(uint32_t)>(reinterpret_cast<const uint32_t*>(&s), sizeof(T) / sizeof(uint32_t));
}

template <typename T, size_t N>
inline constexpr etl::span<uint8_t, dynamic_or_size(N, N * sizeof(T))> span_as_writable_u8(etl::span<T, N> s) {
  return etl::span<uint8_t, dynamic_or_size(N, N * sizeof(T))>(reinterpret_cast<uint8_t*>(s.data()), s.size_bytes());
}

template <typename T>
inline constexpr etl::span<uint8_t, 1 * sizeof(T)> as_writable_u8(T& s) {
  return etl::span<uint8_t, 1 * sizeof(T)>(reinterpret_cast<uint8_t*>(&s), sizeof(T));
}

}  // namespace vuc
