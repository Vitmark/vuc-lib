/**
 * @file linear.hpp
 *
 * @par
 * Copyright (c) 2024 Vít Vaněček
 * This code is licensed under MIT license (see LICENCE for details)
 */
#pragma once

#include "etl/algorithm.h"
#include "etl/array.h"
#include "etl/functional.h"
#include "vuc/limit.hpp"

namespace vuc {
inline namespace linear {

template <typename TScalar>
struct vec2 {
  constexpr bool operator==(const vec2<TScalar>& other) const = default;

  constexpr vec2<TScalar>& operator+=(const vec2<TScalar>& other) {
    v0 += other.v0;
    v1 += other.v1;
    return *this;
  }
  constexpr vec2<TScalar>& operator+=(const TScalar& scalar) {
    v0 += scalar;
    v1 += scalar;
    return *this;
  }
  constexpr vec2<TScalar> operator+(const vec2<TScalar>& other) const {
    vec2<TScalar> temp(*this);
    temp += other;
    return temp;
  }
  constexpr vec2<TScalar> operator+(const TScalar& scalar) const {
    vec2<TScalar> temp(*this);
    temp += scalar;
    return temp;
  }

  constexpr vec2<TScalar>& operator-=(const vec2<TScalar>& other) {
    v0 -= other.v0;
    v1 -= other.v1;
    return *this;
  }
  constexpr vec2<TScalar>& operator-=(const TScalar& scalar) {
    v0 -= scalar;
    v1 -= scalar;
    return *this;
  }
  constexpr vec2<TScalar> operator-(const vec2<TScalar>& other) const {
    vec2<TScalar> temp(*this);
    temp -= other;
    return temp;
  }
  constexpr vec2<TScalar> operator-(const TScalar& scalar) const {
    vec2<TScalar> temp(*this);
    temp -= scalar;
    return temp;
  }

  constexpr vec2<TScalar>& operator*=(const TScalar& scalar) {
    v0 *= scalar;
    v1 *= scalar;
    return *this;
  }
  constexpr vec2<TScalar> operator*(const TScalar& scalar) const {
    vec2<TScalar> temp(*this);
    temp *= scalar;
    return temp;
  }

  constexpr vec2<TScalar>& operator/=(const TScalar& scalar) {
    v0 /= scalar;
    v1 /= scalar;
    return *this;
  }
  constexpr vec2<TScalar> operator/(const TScalar& scalar) const {
    vec2<TScalar> temp(*this);
    temp /= scalar;
    return temp;
  }

  constexpr vec2<TScalar> operator-() const {
    return vec2<TScalar>{-v0, -v1};
  }

  constexpr vec2<TScalar> operator+() const {
    return vec2<TScalar>{*this};
  }

  TScalar v0;
  TScalar v1;
};

template <typename TScalar>
inline constexpr bool less_v0(const vec2<TScalar>& first, const vec2<TScalar>& second) {
  return first.v0 < second.v0;
}

template <typename TScalar>
inline constexpr bool less_v1(const vec2<TScalar>& first, const vec2<TScalar>& second) {
  return first.v1 < second.v1;
}

enum class quadrant {
  q1 = 1,
  q2 = 2,
  q3 = 3,
  q4 = 4,
};

template <typename TScalar>
inline constexpr quadrant get_slope_vec_quadrant(const vec2<TScalar>& vec_0, const vec2<TScalar>& vec_1) {
  if (vec_1.v0 > vec_0.v0 && vec_1.v1 > vec_0.v1) {
    return quadrant::q1;
  } else if (vec_1.v0 < vec_0.v0 && vec_1.v1 > vec_0.v1) {
    return quadrant::q2;
  } else if (vec_1.v0 > vec_0.v0 && vec_1.v1 < vec_0.v1) {
    return quadrant::q4;
  } else {
    return quadrant::q3;
  }
}

template <typename TScalar>
inline constexpr bool lin_func_should_inv(const vec2<TScalar>& vec_0, const vec2<TScalar>& vec_1) {
  const auto q = get_slope_vec_quadrant(vec_0, vec_1);
  return (q == quadrant::q2 || q == quadrant::q4);
}

template <typename TScalar>
inline constexpr vec2<TScalar> get_unsigned_slope_vec(const vec2<TScalar>& vec_0, const vec2<TScalar>& vec_1) {
  const auto quad = get_slope_vec_quadrant(vec_0, vec_1);
  switch (quad) {
    case quadrant::q1:
    default: {
      return vec_1 - vec_0;
    }
    case quadrant::q2: {
      return {vec_0.v0 - vec_1.v0, vec_1.v1 - vec_0.v1};
    }
    case quadrant::q3: {
      return vec_0 - vec_1;
    }
    case quadrant::q4: {
      return {vec_1.v0 - vec_0.v0, vec_0.v1 - vec_1.v1};
    }
  }
}

template <typename TScalar>
inline constexpr vec2<TScalar> get_unsigned_offset_vec(const vec2<TScalar>& vec_0, const vec2<TScalar>& vec_1) {
  const auto q = get_slope_vec_quadrant(vec_0, vec_1);
  if (q == quadrant::q2 || q == quadrant::q3) {
    return vec_1;
  } else {
    return vec_0;
  }
}

template <typename TScalar>
inline constexpr TScalar calculate_linfunc(const vec2<TScalar>& slope_vector, const vec2<TScalar>& offset_vector, const TScalar x) {
  return ((x - offset_vector.v0) * slope_vector.v1) / slope_vector.v0 + offset_vector.v1;
}

template <typename TScalar>
inline constexpr TScalar calculate_linfunc_inv(const vec2<TScalar>& inv_slope_vector, const vec2<TScalar>& offset_vector, const TScalar x) {
  return offset_vector.v1 - ((x - offset_vector.v0) * inv_slope_vector.v1) / inv_slope_vector.v0;
}

template <typename TScalar>
  requires etl::is_unsigned_v<TScalar>
struct u_limited_linear_function : public etl::unary_function<TScalar, TScalar> {
  constexpr u_limited_linear_function() = default;
  constexpr u_limited_linear_function(vec2<TScalar> vec_0, vec2<TScalar> vec_1)
      : slope_vector{get_unsigned_slope_vec(vec_0, vec_1)},
        offset_vector{get_unsigned_offset_vec(vec_0, vec_1)},
        x_limits{etl::min(vec_0.v0, vec_1.v0), etl::max(vec_0.v0, vec_1.v0)},
        inv{lin_func_should_inv(vec_0, vec_1)} {
  }

  constexpr TScalar operator()(const TScalar x) const {
    const TScalar limited_x = vuc::value_in_limit(x_limits.v0, x_limits.v1, x);
    if (inv) {
      return calculate_linfunc_inv(slope_vector, offset_vector, limited_x);
    } else {
      return calculate_linfunc(slope_vector, offset_vector, limited_x);
    }
  }

 private:
  vec2<TScalar> slope_vector;
  vec2<TScalar> offset_vector;
  vec2<TScalar> x_limits;
  bool inv;
};

template <typename TScalar, vec2<TScalar> VEC0, vec2<TScalar> VEC1>
  requires etl::is_unsigned_v<TScalar>
struct cu_limited_linear_function : public etl::unary_function<TScalar, TScalar> {
  static_assert(VEC0.v0 != VEC1.v0, "Function would divide by 0");
  static constexpr vec2<TScalar> slope_vector{get_unsigned_slope_vec(VEC0, VEC1)};
  static constexpr vec2<TScalar> offset_vector{get_unsigned_offset_vec(VEC0, VEC1)};
  static constexpr vec2<TScalar> x_limits{etl::min(VEC0.v0, VEC1.v0), etl::max(VEC0.v0, VEC1.v0)};
  static constexpr bool inv{lin_func_should_inv(VEC0, VEC1)};

  constexpr TScalar operator()(const TScalar x) const {
    const TScalar limited_x = vuc::value_in_limit(x_limits.v0, x_limits.v1, x);
    if constexpr (inv) {
      return calculate_linfunc_inv(slope_vector, offset_vector, limited_x);
    } else {
      return calculate_linfunc(slope_vector, offset_vector, limited_x);
    }
  }
};

template <typename TScalar>
  requires etl::is_signed_v<TScalar>
struct linear_function : public etl::unary_function<TScalar, TScalar> {
  constexpr linear_function() = default;
  constexpr linear_function(vec2<TScalar> vec_0, vec2<TScalar> vec_1) : slope_vector{vec_1 - vec_0}, offset_vector{vec_0} {
  }

  constexpr TScalar operator()(const TScalar x) const {
    return calculate_linfunc(slope_vector, offset_vector, x);
  }

 private:
  vec2<TScalar> slope_vector;
  vec2<TScalar> offset_vector;
};

template <typename TScalar>
  requires etl::is_signed_v<TScalar>
struct limited_linear_function : public etl::unary_function<TScalar, TScalar> {
  constexpr limited_linear_function() = default;
  constexpr limited_linear_function(vec2<TScalar> vec_0, vec2<TScalar> vec_1)
      : slope_vector{vec_1 - vec_0}, offset_vector{vec_0}, x_limits{etl::min(vec_0.v0, vec_1.v0), etl::max(vec_0.v0, vec_1.v0)} {
  }

  constexpr TScalar operator()(const TScalar x) const {
    const TScalar limited_x = vuc::value_in_limit(x_limits.v0, x_limits.v1, x);
    return calculate_linfunc(slope_vector, offset_vector, limited_x);
  }

 private:
  vec2<TScalar> slope_vector;
  vec2<TScalar> offset_vector;
  vec2<TScalar> x_limits;
};

template <typename TScalar, size_t NPoints>
  requires etl::is_signed_v<TScalar>
struct piecewise_linear_function : public etl::unary_function<TScalar, TScalar> {
  using points_array_t     = etl::array<vec2<TScalar>, NPoints>;
  using scalar_array_t     = etl::array<TScalar, NPoints>;
  using linear_functions_t = etl::array<linear_function<TScalar>, NPoints - 1>;

  static constexpr scalar_array_t generate_input_ranges(const points_array_t& points) {
    scalar_array_t ranges{};
    for (size_t i{0}; i < NPoints; i += 1) {
      ranges._buffer[i] = points._buffer[i].v0;
    }
    return ranges;
  }

  static constexpr linear_functions_t generate_linear_functions(const points_array_t& points) {
    linear_functions_t functions{};
    for (size_t i{0}; i < linear_functions_t::SIZE; i += 1) {
      functions._buffer[i] = {points[i], points[i + 1]};
    }
    return functions;
  }

  constexpr piecewise_linear_function(const points_array_t& points)
      : input_ranges{generate_input_ranges(points)}, linear_functions{generate_linear_functions(points)} {
  }

  constexpr size_t get_range_index(const TScalar x) const {
    const auto rng_elemet = etl::lower_bound(input_ranges._buffer, input_ranges.end(), x);
    return rng_elemet - input_ranges.begin();
  }

  constexpr size_t get_linear_function_index(const size_t range_index) const {
    if (range_index == 0) {
      return 0;
    } else if (range_index >= NPoints) {
      return range_index - 2;
    } else {
      return range_index - 1;
    }
  }

  constexpr TScalar operator()(const TScalar x) const {
    const size_t index = get_linear_function_index(get_range_index(x));
    return linear_functions[index](x);
  }

  scalar_array_t input_ranges;
  linear_functions_t linear_functions;
};

template <typename TScalar, size_t NPoints>
  requires etl::is_signed_v<TScalar>
struct limited_piecewise_linear_function : etl::unary_function<TScalar, TScalar> {
  using points_array_t = etl::array<vec2<TScalar>, NPoints>;

  static constexpr vec2<TScalar> get_x_limits(const points_array_t& points) {
    const auto [min, max] = etl::minmax_element(points._buffer, points.end(), less_v0<TScalar>);
    return {min->v0, max->v0};
  }

  constexpr limited_piecewise_linear_function(const points_array_t& points) : piecewise_function{points}, x_limits{get_x_limits(points)} {
  }

  constexpr TScalar operator()(const TScalar x) const {
    const TScalar limited_x = vuc::value_in_limit(x_limits.v0, x_limits.v1, x);
    return piecewise_function(limited_x);
  }

  piecewise_linear_function<TScalar, NPoints> piecewise_function;
  vec2<TScalar> x_limits;
};

}  // namespace linear
}  // namespace vuc
