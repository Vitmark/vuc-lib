/**
 * @file can_process.hpp
 * @brief Functions for processing can_frame
 *
 * @par
 * Copyright (c) 2024 Vít Vaněček
 * This code is licensed under MIT license (see LICENCE for details)
 */
#pragma once
#include <etl/array.h>
#include <etl/endianness.h>
#include <etl/type_traits.h>

#include <cstddef>
#include <cstring>

#include "can.hpp"
#include "encode_decode.hpp"
#include "literals.hpp"

namespace vuc {
inline namespace can {

template <typename... Ts>
inline size_t encode_to_can_data_endian(can_data& data, const etl::endian endianness, const Ts&... values) {
  static_assert(count_bytes_v<Ts...> <= can_data::SIZE, "Size of values overflow can data");
  return encode_byte_array(data, endianness, 0, values...);
}

template <typename... Ts>
inline size_t encode_to_can_data(can_data& data, const Ts&... values) {
  return encode_to_can_data_endian(data, etl::endian::native, values...);
}

#if ETL_USING_CPP20
template <typename T>
  requires(std::is_standard_layout_v<T> && (sizeof(T) <= can_data::SIZE))
inline size_t encode_struct_to_can_data(can_data& data, const T& value) {
  memcpy(data.begin(), &value, sizeof(T));
  return sizeof(T);
}

template <typename T>
  requires(std::is_standard_layout_v<T> && (sizeof(T) <= can_data::SIZE))
inline size_t decode_struct_from_can_data(const can_data& data, T& value) {
  memcpy(&value, data.begin(), sizeof(T));
  return sizeof(T);
}

template <typename T>
  requires(std::is_standard_layout_v<T> && (sizeof(T) <= can_data::SIZE))
[[nodiscard]] inline T create_struct_from_can_data(const can_data& data) {
  T created_struct{};
  decode_struct_from_can_data(data, created_struct);
  return created_struct;
}
#endif

template <typename... Ts>
inline can_data create_can_data_endian(etl::endian endianness, const Ts&... values) {
  can_data new_data;
  encode_to_can_data_endian(new_data, endianness, values...);
  return new_data;
}

template <typename... Ts>
inline can_data create_can_data(const Ts&... values) {
  return create_can_data_endian(etl::endian::native, values...);
}

template <typename... Ts>
inline size_t encode_to_can_frame_endian(can_frame& frame, etl::endian endianness, const Ts&... values) {
  frame.len = count_bytes_v<Ts...>;
  return encode_to_can_data_endian(frame.data, endianness, values...);
}

template <typename... Ts>
inline size_t encode_to_can_frame(can_frame& frame, const Ts&... values) {
  return encode_to_can_frame_endian(frame, etl::endian::native, values...);
}

template <typename... Ts>
inline can_frame create_can_frame_endian(const can_id& id, etl::endian endianness, const Ts&... values) {
  can_frame new_frame{id, 0, {}};
  encode_to_can_frame_endian(new_frame, endianness, values...);
  return new_frame;
}

template <typename... Ts>
inline can_frame create_can_frame(const can_id& id, const Ts&... values) {
  return create_can_frame_endian(id, etl::endian::native, values...);
}

template <typename... Ts>
inline size_t push_to_frame_endian(can_frame& frame, const etl::endian endianness, const Ts&... values) {
  static_assert(count_bytes_v<Ts...> <= can_data::SIZE, "Size of values overflow can data");
  if (count_bytes_v<Ts...> + frame.len > can_data::SIZE) {
    return 0;
  }
  const size_t byte_offset = frame.len;
  const size_t write_size  = encode_byte_array(frame.data, endianness, byte_offset, values...);
  frame.len                = (etl::min)(8_uz, byte_offset + write_size);
  return write_size;
}

template <typename... Ts>
inline size_t push_to_frame(can_frame& frame, const Ts&... values) {
  return push_to_frame_endian(frame, etl::endian::native, values...);
}

template <typename... Ts>
inline bool decode_can_data_offset(const can_data& data, const etl::endian endianness, const size_t byte_offset, Ts&... values) {
  static_assert(count_bytes_v<Ts...> <= can_data::SIZE, "Size of values overflow can data");
  if (count_bytes_v<Ts...> + byte_offset > can_data::SIZE) {
    return false;
  }
  return decode_byte_array(data, endianness, byte_offset, values...);
}

template <typename... Ts>
inline bool decode_can_data_endian(const can_data& data, const etl::endian endianness, Ts&... values) {
  return decode_can_data_offset(data, endianness, 0, values...);
}

template <typename... Ts>
inline bool decode_can_data(const can_data& data, Ts&... values) {
  return decode_can_data_endian(data, etl::endian::native, values...);
}

template <typename... Ts>
inline bool pop_from_can_frame_endian(can_frame& frame, const etl::endian endianness, Ts&... values) {
  if (count_bytes_v<Ts...> > frame.len || frame.id.is_remote()) {
    return false;
  }
  frame.len -= count_bytes_v<Ts...>;
  return decode_byte_array(frame.data, endianness, frame.len, values...);
}

template <typename... Ts>
inline bool pop_from_can_frame(can_frame& frame, Ts&... values) {
  return pop_from_frame_endian(frame, etl::endian::native, values...);
}

}  // namespace can
}  // namespace vuc
