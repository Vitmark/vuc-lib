
/**
 * @file ltc68042.hpp
 * @brief Class for communication with LTC6804-2
 *
 * @par
 * Copyright (c) 2023 Vít Vaněček
 * This code is licensed under MIT license (see LICENCE for details)
 */

#pragma once

#include <stddef.h>
#include <stdint.h>

#include "etl/algorithm.h"
#include "etl/array.h"
#include "etl/byte_stream.h"
#include "etl/expected.h"
#include "etl/span.h"
#include "vuc/defer.hpp"

namespace vuc {

class ltc68042_crc {
 public:
  static constexpr uint16_t initial = 16;
  static constexpr uint16_t poly    = 50585;
  static constexpr auto precomputed = []() constexpr {
    etl::array<uint16_t, 256> out_arr{};

    for (size_t i{0}; i < out_arr.size(); ++i) {
      uint16_t crc = i << 7;

      for (size_t j{0}; j < 8; ++j) {
        crc <<= 1;
        if ((crc & 0x8000) > 0) {
          crc = crc ^ poly;
        }
      }

      out_arr._buffer[i] = crc & 0xFFFF;
    }

    return out_arr;
  }();

  static constexpr uint16_t accumulate(const uint16_t initial_crc, const etl::span<const uint8_t> data) {
    uint16_t crc = initial_crc >> 1;
    for (uint8_t elem : data) {
      uint8_t index = ((crc >> 7) ^ elem) & 0xFF;
      crc           = (crc << 8) ^ precomputed[index];
    }
    return crc << 1;
  }

  static constexpr uint16_t calculate(const etl::span<const uint8_t> data) {
    uint16_t crc = initial;
    for (uint8_t elem : data) {
      uint8_t index = ((crc >> 7) ^ elem) & 0xFF;
      crc           = (crc << 8) ^ precomputed[index];
    }
    return crc << 1;
  }
};

template <typename TSPI, typename TCRC>
class ltc68042 {
 public:
  enum class error {
    pec = 1,
    arguments,
    spi_read,
    spi_write,
  };

 public:
  enum class cmd_base : uint16_t {
    UNKNOWN = 0,
    WRCFG   = 0b00000000001,  // Write Configuration Register Group
    RDCFG   = 0b00000000010,  // Read Configuration Register Group
    RDCVA   = 0b00000000100,  // Read Cell Voltage Register Group A
    RDCVB   = 0b00000000110,  // Read Cell Voltage Register Group B
    RDCVC   = 0b00000001000,  // Read Cell Voltage Register Group C
    RDCVD   = 0b00000001010,  // Read Cell Voltage Register Group D
    RDAUXA  = 0b00000001100,  // Read Auxiliary Register Group A
    RDAUXB  = 0b00000001110,  // Read Auxiliary Register Group B
    RDSTATA = 0b00000010000,  // Read Status Register Group A
    RDSTATB = 0b00000010010,  // Read Status Register Group B
    ADCV    = 0b01001100000,  // Start Cell Voltage ADC Conversion and Poll Status
    ADOW    = 0b01000101000,  // Start Open Wire ADC Conversion and Poll Status
    CVST    = 0b01000000111,  // Start Self-Test Cell Voltage Conversion and Poll Status
    ADAX    = 0b10001100000,  // Start GPIOs ADC Conversion and Poll Status
    AXST    = 0b10000000111,  // Start Self-Test GPIOs Conversion and Poll Status
    ADSTAT  = 0b10001101000,  // Start Status group ADC Conversion and Poll Status
    STATST  = 0b10000001111,  // Start Self-Test Status group Conversion and Poll Status
    ADCVAX  = 0b10001101111,  // Start Combined Cell Voltage and GPIO1, GPIO2 Conversion and Poll Status
    CLRCELL = 0b11100010001,  // Clear Cell Voltage Register Group
    CLRAUX  = 0b11100010010,  // Clear Auxiliary Register Group
    CLRSTAT = 0b11100010011,  // Clear Status Register Group
    PLADC   = 0b11100010100,  // Poll ADC Conversion Status
    DIAGN   = 0b11100010101,  // Diagnose MUX and Poll Status
    WRCOMM  = 0b11100100001,  // Write COMM Register Group
    RDCOMM  = 0b11100100010,  // Read COMM Register Group
    STCOMM  = 0b11100100011,  // Start I2C/SPI Communication
  };

  enum class reg_group_r {
    config,
    cell_voltage_a,
    cell_voltage_b,
    cell_voltage_c,
    cell_voltage_d,
    aux_a,
    aux_b,
    status_a,
    status_b,
    comm,
  };

  enum class reg_group_w {
    config,
    comm,
  };

  static constexpr size_t cmd_bit_md  = 7;
  static constexpr size_t cmd_bit_dcp = 4;
  static constexpr size_t cmd_bit_pup = 6;
  static constexpr size_t cmd_bit_st  = 5;

  enum class adc_mode : uint16_t {
    fast_27khz_14khz   = 0b01 << cmd_bit_md,
    normal_7khz_3khz   = 0b10 << cmd_bit_md,
    filtered_26hz_2khz = 0b11 << cmd_bit_md,
  };

  enum class cell_selection : uint16_t {
    all_cells = 0b000,
    cell_1_7  = 0b001,
    cell_2_8  = 0b010,
    cell_3_9  = 0b011,
    cell_4_10 = 0b100,
    cell_5_11 = 0b101,
    cell_6_12 = 0b110,
  };

  enum class status_selection : uint16_t {
    all_stat = 0b000,
    soc      = 0b001,
    itmp     = 0b010,
    va       = 0b011,
    vd       = 0b100,
  };

  enum class self_test_sel : uint16_t {
    mode_1 = 0b01 << cmd_bit_st,
    mode_2 = 0b10 << cmd_bit_st,
  };

  enum class gpio_selection : uint16_t {
    all_gpio_ref2 = 0b000,
    gpio_1        = 0b001,
    gpio_2        = 0b010,
    gpio_3        = 0b011,
    gpio_4        = 0b100,
    gpio_5        = 0b101,
    ref2          = 0b110,
  };

  enum class stat_group_selection : uint16_t {
    all  = 0b000,
    soc  = 0b001,  // Sum of Cells Measurements
    itmp = 0b010,  // Internal Die Temperature
    va   = 0b011,  // Analog Power Supply Voltage
    vd   = 0b100,  // Digital Power Supply Voltage
  };

 private:
  static constexpr etl::array<cmd_base, 10> reg_read_cmd_lookup{
      cmd_base::RDCFG,  cmd_base::RDCVA,  cmd_base::RDCVB,   cmd_base::RDCVC,   cmd_base::RDCVD,
      cmd_base::RDAUXA, cmd_base::RDAUXB, cmd_base::RDSTATA, cmd_base::RDSTATB, cmd_base::RDCOMM,
  };

  static constexpr cmd_base get_reg_read_cmd(reg_group_r group) {
    const size_t index = (etl::min)(static_cast<size_t>(group), reg_read_cmd_lookup.size() - 1);
    return reg_read_cmd_lookup[index];
  }

  static constexpr cmd_base get_reg_write_cmd(reg_group_w group) {
    switch (group) {
      case reg_group_w::config:
        return cmd_base::WRCFG;
      case reg_group_w::comm:
        return cmd_base::WRCOMM;
      default:
        return cmd_base::UNKNOWN;
    }
  }

 public:
  static constexpr int raw_volt_denominator     = 10000;  // 100uV -> 1V
  static constexpr int raw_milivolt_denominator = 10;     // 100uV -> 1mV
  static constexpr float raw_volt_multiplier    = 1.0f / raw_volt_denominator;

  [[nodiscard]] static constexpr float ltc_raw_voltage_to_float(uint32_t raw_voltage) {
    return raw_voltage * raw_volt_multiplier;
  }

 public:
  constexpr ltc68042(TSPI& writer_, TCRC& crc_) : writer(writer_), crc_calc(crc_) {
  }

  etl::expected<void, error> start_cell_measurement(bool broadcast, uint8_t address, adc_mode mode, bool discharge_permitted, cell_selection cells) {
    const uint16_t cmd = cmd_address(broadcast, address) | (uint16_t)cmd_base::ADCV | (uint16_t)mode | discharge_permitted << cmd_bit_dcp | (uint16_t)cells;
    return send_simple_cmd(cmd);
  }

  etl::expected<void, error> start_gpio_measurement(bool broadcast, uint8_t address, adc_mode mode, gpio_selection gpios) {
    const uint16_t cmd = cmd_address(broadcast, address) | (uint16_t)cmd_base::ADAX | (uint16_t)mode | (uint16_t)gpios;
    return send_simple_cmd(cmd);
  }

  etl::expected<void, error> start_stat_measurement(bool broadcast, uint8_t address, adc_mode mode, status_selection stats) {
    const uint16_t cmd = cmd_address(broadcast, address) | (uint16_t)cmd_base::ADSTAT | (uint16_t)mode | (uint16_t)stats;
    return send_simple_cmd(cmd);
  }

  etl::expected<void, error> clear_cell_voltage(bool broadcast, uint8_t address = 0) {
    const uint16_t cmd = cmd_address(broadcast, address) | (uint16_t)cmd_base::CLRCELL;
    return send_simple_cmd(cmd);
  }

  etl::expected<void, error> clear_aux(bool broadcast, uint8_t address = 0) {
    const uint16_t cmd = cmd_address(broadcast, address) | (uint16_t)cmd_base::CLRAUX;
    return send_simple_cmd(cmd);
  }

  etl::expected<void, error> clear_status(bool broadcast, uint8_t address = 0) {
    const uint16_t cmd = cmd_address(broadcast, address) | (uint16_t)cmd_base::CLRSTAT;
    return send_simple_cmd(cmd);
  }

  etl::expected<etl::array<uint8_t, 6>, error> read_reg_group(uint8_t address, reg_group_r group) {
    const uint16_t cmd = cmd_address(false, address) | (uint16_t)get_reg_read_cmd(group);
    const etl::array<uint8_t, 4> tx_msg{construct_cmd_msg(cmd)};
    etl::array<uint8_t, 8> rx_data{};

    {
      writer.start();
      auto def0 = vuc::defer([&] { writer.end(); });
      if (writer.write(tx_msg) != tx_msg.size()) {
        return etl::unexpected(error::spi_write);
      }
      if (writer.read(rx_data) != rx_data.size()) {
        return etl::unexpected(error::spi_read);
      }
    }

    const uint16_t received_pec   = rx_data[6] << 8 | rx_data[7];
    const uint16_t calculated_pec = crc_calc.calculate({rx_data.begin(), 6});
    if (received_pec != calculated_pec) {
      return etl::unexpected(error::pec);
    }

    etl::array<uint8_t, 6> out_regs;
    etl::copy_n(rx_data.begin(), 6, out_regs.begin());
    return out_regs;
  }

  etl::expected<void, error> write_reg_group(uint8_t address, etl::span<const uint8_t> data, reg_group_w group = reg_group_w::config) {
    if (data.size() < 6) {
      return etl::unexpected(error::arguments);
    }
    const uint16_t cmd = cmd_address(false, address) | (uint16_t)get_reg_write_cmd(group);
    const etl::array<uint8_t, 4> tx_msg{construct_cmd_msg(cmd)};
    if (data.size() > 6) {
      data = data.subspan(0, 6);
    }
    const uint16_t crc = crc_calc.calculate(data);
    const etl::array<uint8_t, 2> crc_array{(uint8_t)(crc >> 8), (uint8_t)(crc)};

    {
      writer.start();
      auto def0 = vuc::defer([&] { writer.end(); });
      if (writer.write(tx_msg) != tx_msg.size()) {
        return etl::unexpected(error::spi_write);
      }
      if (writer.write(data) != data.size()) {
        return etl::unexpected(error::spi_write);
      }
      if (writer.write(crc_array) != crc_array.size()) {
        return etl::unexpected(error::spi_write);
      }
    }

    return {};
  }

 private:
  etl::array<uint8_t, 4> construct_cmd_msg(const uint16_t cmd) {
    etl::array<uint8_t, 4> tx_msg;
    etl::byte_stream_writer msg_stream{tx_msg, etl::endian::big};
    msg_stream.write(cmd);
    uint16_t crc = crc_calc.calculate({tx_msg.begin(), msg_stream.size_bytes()});
    msg_stream.write(crc);
    return tx_msg;
  }

  etl::expected<void, error> send_simple_cmd(uint16_t cmd) {
    const etl::array<uint8_t, 4> tx_msg{construct_cmd_msg(cmd)};

    writer.start();
    auto def0 = vuc::defer([&] { writer.end(); });
    if (writer.write(tx_msg) != tx_msg.size()) {
      return etl::unexpected(error::spi_write);
    }

    return {};
  }

  static constexpr uint16_t cmd_address(bool broadcast, uint8_t address) {
    if (broadcast) {
      return 0;
    } else {
      address = address & 0x0F;
      return 1 << 15 | address << 11;
    }
  }

  /**
   * @param adcopt ADC Mode Option bit:
   *               - false -> 27kHz, 7kHz, 26kHz
   *               - true  -> 14kHz, 3kHz, 2kHz
   * @param swtrd Reference Powered Up:
   *               - false -> Reference Shuts Down after Conversions
   *               - true  -> Reference Remains Powered Up Until Watchdog Timeout
   */
  static constexpr uint8_t construct_cfgr0(bool adcopt, bool refcon, uint8_t gpio_pullup = 0) {
    return adcopt | refcon << 2 | (gpio_pullup & 0x1F) << 3;
  }

 private:
  TSPI& writer;
  TCRC& crc_calc;
};

}  // namespace vuc
