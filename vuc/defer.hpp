/**
 * @file defer.hpp
 *
 * @par
 * Copyright (c) 2024 Vít Vaněček
 * This code is licensed under MIT license (see LICENCE for details)
 */
#pragma once

namespace vuc {

template <typename F>
struct deferrer {
  F f;
  ~deferrer() {
    f();
  }
};

template <typename F>
inline constexpr deferrer<F> defer(F f) {
  return {f};
}

}  // namespace vuc
