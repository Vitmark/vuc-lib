/**
 * @file bitset.hpp
 *
 * @par
 * Copyright (c) 2024 Vít Vaněček
 * This code is licensed under MIT license (see LICENCE for details)
 */
#pragma once

#include <cstddef>

#include "etl/algorithm.h"
#include "etl/integral_limits.h"

namespace vuc {

template <typename TVal>
  requires etl::is_integral_v<TVal>
[[nodiscard]] constexpr bool test_bit(const TVal value, size_t position) {
  const TVal mask = TVal(TVal(1) << position);
  return (value & mask) != TVal(0);
}

template <typename TVal>
  requires etl::is_integral_v<TVal>
[[nodiscard]] constexpr TVal set_bit(TVal value, size_t position) {
  const TVal mask = TVal(TVal(1) << position);
  value |= mask;
  return value;
}

template <typename TVal>
  requires etl::is_integral_v<TVal>
[[nodiscard]] constexpr TVal reset_bit(TVal value, size_t position) {
  const TVal mask = TVal(TVal(1) << position);
  value &= ~mask;
  return value;
}

template <typename TVal>
  requires etl::is_integral_v<TVal>
[[nodiscard]] constexpr TVal flip_bit(TVal value, size_t position) {
  const TVal mask = TVal(TVal(1) << position);
  value ^= mask;
  return value;
}

template <typename TVal>
  requires etl::is_integral_v<TVal>
[[nodiscard]] constexpr TVal change_bit(TVal value, size_t position, bool change) {
  if (change) {
    return set_bit(value, position);
  } else {
    return reset_bit(value, position);
  }
}

template <typename TVal>
  requires etl::is_integral_v<TVal>
struct bitset {
  static constexpr auto bit_mask  = etl::integral_limits<TVal>::max;
  static constexpr auto bit_count = etl::integral_limits<TVal>::bits;

  TVal value{TVal(0)};

  constexpr bool none() const {
    return value == TVal(0);
  }

  constexpr bool any() const {
    return value != TVal(0);
  }

  constexpr bool all() const {
    return value == bit_mask;
  }

  constexpr bool test(size_t position) const {
    return test_bit(value, position);
  }

  constexpr void set(size_t position) {
    value = set_bit(value, position);
  }

  constexpr void reset(size_t position) {
    value = reset_bit(value, position);
  }

  constexpr void flip(size_t position) {
    value = flip_bit(value, position);
  }

  constexpr void change(size_t position, bool change) {
    value = change_bit(value, position, change);
  }

  constexpr void set_all() {
    value = bit_mask;
  }

  constexpr void reset_all() {
    value = TVal(0);
  }

  constexpr void flip_all() {
    value = ~value;
  }

  constexpr bool operator==(const TVal& other) {
    return value == other;
  }

  constexpr bool operator==(const bitset<TVal>& other) {
    return value == other.value;
  }

  struct bit_reference {
    const size_t position;
    bitset<TVal>& rbitset;

    constexpr bit_reference(size_t position, bitset<TVal>& rbitset) : position(position), rbitset(rbitset){};
    constexpr bit_reference(const bit_reference& other) = default;

    constexpr bit_reference& operator=(bool value) {
      rbitset.change(position, value);
      return *this;
    }

    constexpr bit_reference& operator=(const bit_reference& other) {
      rbitset.change(position, bool(other));
      return *this;
    }

    constexpr operator bool() const {
      return rbitset.test(position);
    }

    constexpr bool operator~() const {
      return !rbitset.test(position);
    }
  };

  constexpr bit_reference operator[](size_t position) {
    return {position, *this};
  }

  constexpr bool operator[](size_t position) const {
    return test(position);
  }
};

}  // namespace vuc
